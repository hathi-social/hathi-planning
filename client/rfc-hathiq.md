# Table of Contents

1.  [Decisions](#orgb87d9be)
    1.  [Formalia](#orge3de0db)
        1.  [Name](#org0571822)
        2.  [Scope](#orgb7041b9)
        3.  [Standards](#org440f8d1)
    2.  [Architecture](#orga90d90e)
        1.  [Communication](#org2fc7aa2)
        2.  [Key storage](#orgac5215e)
        3.  [Data representation](#org3230782)
        4.  [Modules](#org81c8b8b)
    3.  [Commands](#org8e91da5)
        1.  [Command description structure](#orga00bd0b)
        2.  [List of commands](#orgba4b900)
2.  [Unclear/To be decided](#orgb6e913c)


<a id="orgb87d9be"></a>

# Decisions


<a id="orge3de0db"></a>

## Formalia


<a id="org0571822"></a>

### Name

hathiq


<a id="orgb7041b9"></a>

### Scope
- Minimal client modeled after curl and ntpq
- Client responsibility is connecting to actor inbox and actor outbox; server will distribute (no complicated logic)
- Client limited to one server and user at a time
- This client is initially intended for testing only, later its primary focus will be for use with scripts
- Later clients: Build text-mode interface, using same server library as the test client
- Future clients will have multiple parallel sessions, but that is outside hathiq's design function


<a id="org440f8d1"></a>

### Standards

- Pep8
  - Programs pep8 and pyflakes check syntax and style


<a id="orga90d90e"></a>

## Architecture


<a id="org2fc7aa2"></a>

### Communication

-   Client should encrypt every payload; https should suffice
-   Authentication
    -   Method not decided
    -   implement as automatically passing for now
-   Post-authentication
    -   Https GET
    -   Https POST
-   Sending something
    -   Client issues a POST request to its Actor's Outbox
    -   Server will pass message from Outbox to recipient(s)
-   Request objects by id/url
    -   How?
    -   Depends on details of the interface.
    -       Direct request for an object ID (aka URL)
    -       By reference via already known objects? (ex: Me.inbox)


<a id="orgac5215e"></a>

### Key storage

-   Interchange format needs to be defined
-   Do not store in a registry; it is a Bad Idea (unexplained but likely refers to unintended obscuration of information)
-       TL;DR explanation: Binary formats trade flexibility and robustness for efficiency (and that only with well designed ones). They are premature optimization.
-   Import/export system
    -   Easy to use
    -   User should not be confronted with details
    -   *"Even devs don't like poking around in config folders, Aunt Tillie will put that in the same category as climbing Mt. Doom while carrying a pack of rabid piranhas and hanging over a Sarlacc pit."* [typos corrected]
-   Need to be able to:
    -   Authenticate (how?)


<a id="org3230782"></a>

### Data representation

-   Data representation of ActivityPub objects
    -   Use one single class since it's all json; for testing, nothing more is needed (agreed upon between Nicolas and Ian)
    -   Roughly speaking; the client receives a JSON string, sends it to python's json module, and either uses the dict directly or slurps the data into some sort of HathiObject class


<a id="org81c8b8b"></a>

### Modules

-   3 planned clients
    -   hathiq
    -   hathi-curses
    -   basic gui client
    -   will use same libraries
-   Client library is likely to be separated out from the code along the way. Don't know proper seperation till implementation.


<a id="org8e91da5"></a>

## Commands

-   **Use argparse for dealing with repl commands:** <https://docs.python.org/3.3/library/argparse.html>
-   commandline option '-c': handles any command that repl could handle; this implies that every command must be able to get all of its data from a single line though.
-   password must not be passed as command-line parameter (cleartext), use python's getpass module. (this only applies if we are using passwords)


<a id="orga00bd0b"></a>

### Command description structure [uh, not quite sure what this is - IB]

-   command line name
-   repl name
-   general structure: <command> <object type>:<title> <recipient>, [data input]?
-   Clear description of consequences
-   Expected feedback from command


<a id="orgba4b900"></a>

### List of commands

-   login
-   lso #list Actors owned
-   active <Actor>
-   note <title> <scope/dest>[: subtitle] <&#x2013; input stream
-   follow <Actor>
-   view <Actor>/<box>
-   note : title <&#x2013; input stream  (defaults to public)
    -   UW: shouldn't it default to private or unlisted?
    -   IB: good point, defaults are a policy decision that will have to be made.
-   command needed to request object by id or url
-   command needed to create activities/objects and post them to one's own outbox
-   (stretch) searching, ie requesting certain object-types by other fields
-   connect <user>:<server>
    -   UW comments:
        -   one user having several actors is assumed here?
        -       Yes. The user is what authenticates
        -   should not each actor correspond to one user though?
        -       Every Actor has one user, users own a variable number of Actors.
        -   each have public/private key pair identifying them anyway
        -   one user controlling multiple actors assumes 'master' key pair
        -       Yes/No/Maybe, depends on how you define it. Actors don't auth, only users.


### The confusion around Actors and Users
-    A "User" is the entity that authenticates with a server. Users own "Actors".
-    The clearest way to define a user is as a set of authentication keys controled by someone.
-    Any time a human does something the server checks their user credentials to see is they are allowed to do that thing with that Actor.
-    An "Actor" is a type of ActivityPub object. Every Actor is owned by a user.
-    There are several types of Actors: Application, Group, Organization, Person, Service.
-    The "User" is *not* part of the ActivityPub spec proper, though it is a very important piece of the puzzle.
-    What this means for the hathiq client:
-       The client only works with a single User at a time.
-       The client may work with any number of users at a time.

<a id="orgb6e913c"></a>

# Unclear/To be decided

-   are we going with public key crypto?
-   if so which standard key storage/interchange format should we use?
-   if crypto, what will export/import look like?
-   if crypto: how do we authenticate?

