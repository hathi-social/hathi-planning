# Hathi Command-line Client

## Title and People
Authour: Uffe Wassmann  
Reviewer: None  
Last updated: 2019-04-11  

## Overview
This document details the design of a command-line client used to test and communicate with the rest of the Hathi network.  
The command-line client is meant as an easy way to test ideas and features, and also to integrate well with command-line based workflows and scripting.  

## Context
In designing Hathi, we need to test our assumptions. Mistaken assumptions should be realized fast, and good ideas should be verified equally fast.  
This client focuses solely on functionality and can be used to test ideas and assumptions.  

## Goals and Non-Goals
### Goals
- provide a good platform for experiments for new contributors  
- through being scripted, be able to run a service daemon for the network  
- metrics: feedback and use  
- tracking: feedback channel in user section of Hathi discord server  

### Non-Goals
- No pretty pseudo-graphics.  
  Any of the other (planned) clients would be a better choice for that sort of thing.  
- No continuous, synchronous exchanges. This is all a single step.  
  Anything else would hinder scriptability.  

### Milestones

Legend:  
*: Working on  
.: Not started  
o: Researched  
+: Implemented  

- ..... Send/receive communication with server/router established  
- ..... Encryption between client and server/router established  
- ..... Client library spliced out  
- ..... Commands/options interface established  
- oo... Metadata recognition and filtering established  
- ..... All user stories for ICN version implemented  


#### Recent Changes
Anything affecting the planned milestones that they are not currently adjusted for.  
(None)  

### Existing Solution
Similar solutions would be command-line mail and news readers and senders.  
E.g.  
- send message to other user  
- publish a public post  
- get all topics and senders of everything received so far  
- receive message id number xyz  
- get a list of all published posts meeting the filter criteria xyz  
- get a list of all new topics in group xyz  
- retrieve all posts on topic xyz in group zyx  

### Proposed Solution - AKA "Technical Architecture Section"

#### Structure of command-line client
- Client library  
  - Interface to protocol  
  - Raw functions for use by all/most clients  
- Command-line interface  
  Handles commands and their options, then exits program  
  A standard library should be available to handle the parsing of commands and options.  
- Debugging  
  Tests should be available for things like connection, server presence, and client self-verification  
  A standard library should be available for good testing.  
- Metadata parsing  
  For filtering purposes. Every message is scanned for a metadata section.  
  If present, metadata can be used for filtering based on selection criteria given through options.  

#### Example of use

```
hathi-clc --actor=me --server=test.hathi.org -- list
```

1. Asks for a password, which might be provided by a pw-manager.  
- Connects to the server  
- Verifies the actor's identity to the server  
- Retrieves a list of all current messages in actor's inbox  
- Dumps that list as output on terminal, in format of msg_id, subject, sender, date; ordered by topic then date  

#### Example of implementation

1. Options ```--actor=me``` and ```--server=test.hathi.org``` are interpreted by standard library along with the actual command ```list```  
- Main program options are set as configuration variables  
- Control is ceded to the function implemting the ```list``` command  
- Authentication is performed with server  
- Message data is retrieved  
- Message data is filtered according to criteria (here none)  
- Message data is sorted according to default ordering  
- Message data is printed to terminal  

Possible exceptions:  

- Syntax check on parsing  
- Value validations  
- Sufficient parameters to perform the command requested  

### Alternative Solutions
- Server handling of message metadata  
  This would make it possible to filter messages server-side, and avoid unnecessary downloads.  
  It would also mean that the server would be able to read content metadata; in some cases, even that might be too much information for the server network to possess.  
  Server-side definition of metadata could block client features since server development will be more restricted.  
  On the other hand, client-side handling of metadata and encryption essentially makes the clients potentially independent of their underlying network.  
- Using existing solutions  
  Full argument belongs in general hathi design document, but in short, existing solutions were not designed with privacy and collaboration in mind, and they have also become unwieldy in the various attempts to solve extant problems, eg spam.  

### Testability, Monitoring and Alerting
A standard testing library should be used.  
A separate data file with assertion tests would make it easy to modify testing procedures.  
Different sections in the file could run different test series.  
Selfdiagnostic could include suggestions to user on what to do to fix things in case issues are discovered.  
It might be an idea with a 'feedback' command which simply records a message and sends it directly to dev-team inbox. That should give the easiest way of tracking feedback, suggestions and frustrations without resorting to actual monitored use.  

### Cross-Team Impact
No impact on running solution other than to make it available for actual use.  
Might allow us some coordination options as a team once group client daemons are also running.  

### Open Questions - AKA "Known Unknowns"

#### How do I call the protocol library?
- Do I call it?  
- Or should I re-implement its functions in Python?  
- It seems silly to reimplement the same functionality. On the other hand, how easy/difficult is it to use a Go library from Python?  

#### How is 'unread' messages marked vs 'read'?
- Would the server do this?  
- It could make sense for a client to do it; the server perspective is on whether they were sent or not  
- a command-line client has no state  
- ideally, this feature should not be implemented; it's cleaner that way  

#### Separation of program options versus command options
- Git should provide a good example to follow  

#### It should be possible to filter messages without retrieving all of their content
- Traditional metadata-handled-by-server approach achieves this  
- Traditional approach is, however, inflexible and rigidifies both client development and the possibility of multiple client implementations  
- Server could split metadata content out from the rest of the content if necessary  
- With text messages and documents, this is not much of an issue; with thousands of eg videos though.. that split would be necessary  

### Detailed Scoping and Timeline

- *.... Send/receive communication with server/router established  
  - Estimate: 3 points  
- ..... Encryption between client and server/router established  
  - Estimate: 5 points  
- ..... Client library spliced out  
  - Estimate: 2 points  
- ..... Commands/options interface established  
  - Estimate: 2 points  
- oo... Metadata recognition and filtering established  
  - Estimate: 3 points  
- ..... All user stories for ICN version implemented  
  - Estimate: 3 points  

Estimated exhange rate: 3 points, 10 hours.  
Estimated total: 1 week.  
Calculated work ahead, 18 points, ie 60 hours.  
One of these estimates is definitely off.  
