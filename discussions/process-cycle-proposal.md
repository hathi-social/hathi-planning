# Process Cycle
It is quite easy for plans and repository documents to go stale in the repository; discussions of architecture may happen along the way and things may not be written down in the moment.

To ensure that the information in this document is valid and up to date, we will rotate the responsibility between us, in part to ease the burden but also in order to get all of our slightly different perspectives and priorities represented.

At any given time, one person will only be responsible for one of the processes written below.

## Writing (wri)
This is the act of writing new things into the document and keeping it up to date.

## Review (rev)
The reviewer makes sure that the document looks presentable, is understandable, provides a good overview over the project and isn't missing anything important.
In day-to-day affairs, this means ensuring that new writings make sense and that nothing important was removed during the writing.

## Archiving (arc)
During the writing or reviewing process, it might happen that it is decided that it would be more appropriate for parts of the text to reside elsewhere.
The archivist is the one delegated to create the new document or incorporate the information into other, existing documents.

## Accountability (acc)
The person doing this.. is charged with keeping track of who is supposed to do what and making sure that it actually gets done :]
