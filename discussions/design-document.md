*This document succeeds/supercedes/incorporates:*

- *wishlist.md*
- *technical-platform.md*
- *component-overview.md*
- *interservice-apis.md*
- *use-cases.md*
- *user-auth-acl.md*


Project
=======================================

Stakeholder priorities
---------------------------------------

### ICEI Mission Statement

Hathi is a project under ICEI, and its purpose is to support ICEI's mission.
> *ICEI's mission is to support the development and stewardship of reliable, secure, and open source internet infrastructure software*

Hathi is to become the tool FOSS developers need in order to meet, discuss, collaborate and create and maintain the FOSS software of tomorrow.

#### Concept:
Hathi will provide a global network of discussion groups, easily searchable, with functionality to quickly give a good overview of in-depth, long-running and complicated technical discussions, free to join by anyone who wishes, revealing as little or as much about themselves as they wish to, and requiring as little hardware and bandwidth as possible.

Outside forces are likely to try to control, direct and censor the free expression of discussion participants; should they succceed, trust in the network will diminish and participation lowered, and collaboration efforts hindered.
Thus, Hathi will be designed to counter such attempts.

##### Priorities:
* Anonymity is essential
  * Participants must be safe from persecution even if they live within totalitarian regimes
  * In the so-called safe West, game developers are being harassed simply on account of having the wrong politics
  * Early warning of bugs in critical software infrastructure may sometimes only happen if the reporter is assured anonymity
* Encryption is essential
  * Discussions regarding zero-day exploits in critical software infrastructure need to be held discreetly
  * Without encryption, state agents and others could easily nullify participant anonymity
* Decentralization and federation are essential
  * Having single points of failure is not an option
  * Given the low ressources available, processing in the network needs to be spread out
  * Any given server may fail or become compromised; users must be able to migrate seamlessly

#### Goals:
1. Produce a protocol and reference implementations for a secure communications network
  1. The network must be robust in the face of censorship attempts
  2. The network must have high stability and work on unstable connections
  3. The network must support good hierarchical overviews such as threading
  4. The network must provide (directly or through extensions) modern social networking features
  5. Support use of identity servers; minimize logins, simplify user migration
2. Mentor and evaluate new volunteers
  1. Work with and mentor new ICEI software engineering volunteers
  2. Work with and mentor new ICEI staff
3. Outreach to developers and entrepreneurs in developing nations
  1. Support asynchronous technical collaboration
  2. Minimize hardware requirements of official Hathi implementations
  3. Assume work being done through intermittent network connections and mobile clients
4. Attract developers, visionaries and entrepreneurs
  1. Promote free choice in all things
  2. Be transparent and predictable
  3. Support self-governed interest groups
  4. Be minimalistic, utilitarian, and effective
  5. Present a well-documented, stable API for third-party interfaces.

#### Synergies:
Obviously a worldwide collaboration network will be useful for other groups than merely developers; designing Hathi to allow for the widest range of interests possible will have multiple benefits:
* The network will attract more interest  
  More interest means more participants, more networking, and more collaboration.
* Mixed needs will drive innovation  
  With a solid framework in place for them, various groups will make their own
  extensions to Hathi, giving a wide range of developers experience with Hathi and FOSS.
* Attractive to knowledge workers  
  Current media platforms are widely untrusted.  
  A guaranteed censorship-free network will attract knowledge workers.  
  Having knowledgeable ressources participate in the network on a regular basis will
  make it the go-to place for researching and finding in-depth discussions and
  knowledge in any given field.

Preliminary discussions of Hathi primary goals has resulted in a design concept which would accomodate a number of related use cases with little to zero overhead.

These use cases are:
* Non-profit collaboration
* Self-governed groups
* Coordination amongst minority groups
* Backbone to freedom of speech
* Alternative news outlet for people in repressive regimes
* Emergency peer to peer messaging

Supporting these use cases may not be the primary goal of ICEI, but they have broad appeal and encompass priorities and values of the FOSS community; accomodating potential future extensions and modules supporting these use cases in the Hathi design is therefore seen to be of significant strategic value with respect to creating and fostering FOSS communities of the future.


### Development team

#### Primary Priorities

- Reliability
- Understandable
- Maintainability
- Testability

Argument for prioritizing these four priorities: If we have them, our other priorities of reusability and security will be more or less covered

#### Secondary Priorities
- Reusability
- Security (subdivided, since we have many security goals)
  - 1:
  - 2:
  - 3:

License
---------------------------------------
***TBD

Formatting Standards
---------------------------------------

***For Python3: PEP-8, verified by Flake
***For Go: Go provides its own format checker

Platform
=======================================

Hardware
---------------------------------------

### Small servers
Due to our use cases, we need to assume servers of limited ressources and capability. Ideally a Hathi server should be able to run from a Raspberry Pi.

### Intermittent net connection
Due to our use cases, we need to assume that net connections will not be, can not be constant.

### Intermittent power supply
Servers and clients will have sudden power-offs due to blackouts and brownouts.

Software
---------------------------------------

### Server-side language: Golang
Based on developer input on the matter, we had python and go as viable choices.  
Python is well-known and has good readability.  
Go, on the other hand, while less well-known, had an interest from developers and seemed to promise good efficiency since it's specialized for microservices.  
We tested go as server implementation language with the hathi server prototype, and it was deemed a good fit.  

### Server database backend: SQLite vs PostgreSQL vs MySQL
Consensus to attempt general support as much as possible;  
if choices have to be made, prioritize PostgreSQL.
SQLite is too simple to handle our data at scale efficiently.
MySQL would make us dependent on Oracle; MariaDB could be a possible option.
Consensus in team, though, was for PostgreSQL, based on practical experiences.

#### Client-side language: Python3
Based on the fact that we will be wanting as many as possible to participate in client development, and also to provide a good, solid basis for alternative clients to be made.
This along with the other qualities of Python - primarily its developer adoption and readability.

#### Client database backend: ***TBD


Architecture
=======================================

![Diagram](http://www.plantuml.com/plantuml/proxy?src=https://gitlab.com/hathi-social/planning/raw/master/discussions/component-overview.pump)


Components
---------------------------------------

***TBD: Corrections following the inclusion of new architectural modules intended for handling bridging and identity management

### User
A user is a person of flesh and blood, residing in the physical world,
using their device to access the network.  
This definition may seem obvious, but it is about to become important.

### Actors
An Actor is an entity specifically defined by ActivityPub as follows:  
> *Actor types are Object types that are capable of performing activities.*  
> *The core Actor Types include:*  
> - *Application*  
> - *Group*  
> - *Organization*  
> - *Person*  
> - *Service*  

As it can be seen, this concept is closer to being a datatype than it is
to being a real-world entity, but it does turn out to be quite useful in
many ways.  

#### Actor-Person
A basic user can be implemented as an Actor-Person.  
However, a single user can also be represented as any number of
Actor-Persons, e.g. one that faces the world as a website admin, one
which is more private and representing the person to family and friends,
plus one political profile.

#### Actor-Organization
In addition to this, we might have users representing organizations and
thus administering those organizations' Actor-Organization.

#### Actor-Service
Actor-Services can be many things (...)

### Hathi User Client
The User Client connects the user and lets them interact with the rest
of the network.
It is an application under the direct control of a user.

### Hathi Group Client
The Group Client is actually a daemon.
What it does is that it connects periodically to its Server, retrieves
the messages it might have, and then resends those messages to the
groups members.  
Importantly, this Group Client is completely independent of any Server
setup, and no user needs a public-facing ip address in order to run a
group.

### Hathi Service Client
A Service Client, like the Group Client, is actually a daemon.
And the Service Client, too, connects periodically to its Server, and
may retrieve, process and/or send messages.  
However, a Service Client is a broader concept.  
A Service Client can be almost anything.  
A news channel. An IFTTT interface. An IRC bridge. A voting system. A
bitcoin exchange.
The Hathi Development Team is creating an "availability service",
intended for coordination of multiple persons' availability for e.g.
meetings, across multiple time zones, and with fine-grained preferences
possible for the individual user.

### Hathi Server [Core]
The Server is responsible for storage and transportation of objects and
messages in the network.  
This is all the Server does. Propagation of data. And temporary storage.

### Hathi Identity Management Module(s)
***TBD - under discussion
Will probably consist of 1: Identity Manager; 2: Discovery Service and 3: Identity verification through the Session Manager

##### Migration
Identities should be migratable to promote user choice and secure against impersonation by malicious servers.
Following the decision to roll our own protocol, building on ActivityPub (and not following either Zot or Scuttlebot), we need to define our own migrating identities as well.

##### Discoverability:
There are 3 ways of going about this.
1) Distributing address book throughout the servers
2) Address book on blockchain public ledger 
3) Address book as ActivityPub service

### Identity Manager [IDM]
Responsible for authentication, actor creation, etc.

### Session Manager [SESM]

### Comms/Bridge/Translation (name(s) pending)
Various modules that translate between the inter-module protocol and external protocols. Example: HTTP


Processes
---------------------------------------

### ***TBD: Packing/unpacking data structures to/from ActivityPub json

### ****TBD: Translation/bridging to other networks

### Encryption
Hathi will have end-to-end encryption, and it will be based on private/public key pairs
We will have (minimum) two levels of security, basic and expert.
Expert users handle their private keys themselves,
Basic users let the server store their keys and can retrieve them by email-authentication.
To enhance security for basic users, client password can be used to unlock private keys stored server-side so that server access to keys is hindered.

### User Authentication and Access Control

#### Sequences

User authentication and ACL design is based on the following factors:

* Microservice-based architecture.
* Using public-key encryption.

The following sections provides an overview of the communication sequences for authorization and access control.

##### User creation
![User Creation Diagram](user-auth-acl-diagrams/user_creation.png)

A user will use a Hathi Client to create a new account with the Hathi Server they wish to create an account in. During the registration, the user will decide whether to use (this implies the user will generate their own if needed) their own key pair or let Hathi manage the key pair.

For users choosing to manage their own keys, the user will provide the following:

* Username
* The public key encoded in a format for easy transport.

For users choosing to let Hati manage their keys, the user will provide the following:

* Username
* Password to encrypt the private key

##### Authorization
![Authentication Diagram](user-auth-acl-diagrams/auth_sequence.png)

During user authorization the self-managed key user provides the username and private key to a Hathi client; if the private key is encrypted the Hathi Client will prompt the user for the password. For a hathi-managed key, the user provides the username and the private key password. The Hathi Client will ask the Hathi server for the user's encrypted private key.

The Hathi client will initiate user authentication with the Hathi server. The Hathi Server will send down a nonce encrypted with the user's public key. This will be sent back by the Hathi client after re-encrypting the decrypted nonce with the user's private key. If the Hathi Server determines the nonce is correct it will send the success response and the session token; otherwise the server will respond with authentication failed.

##### Access control
![Request with ACL](user-auth-acl-diagrams/request_with_acl.png)

Once a user has been authenticated and a Hathi client is in possession of a session token, requests by the client should include this token to allow the server to determine whether the user has access to the object being requested. If the user associated to the token is authorized, the server will respond with the status of processing the user's requested action. Otherwise, the server will respond with a reauthenticate response. The client should ask the user to login again and initiate the authentication sequence.

For example, if a user wishes to post a note to their outbox the server will verify if the user associated with the session token is authorized to create notes on the outbox. If the user is authorized, the server will respond with the status of processing the note creation. Otherwise, the server will respond with reauthenticate. The client will display the login screen and initiate an authenticate to get a new session token. The previous action can be tried once a new token is available

##### User migration
![User Migration](user-auth-acl-diagrams/user_migration.png)

If a user chooses to migrate their account from one server to another, the user specifies the new instance to migrate to. The user migration will go in three phases: data transfer, data import to the new server, redirect references from the old instance to the new instance.

In the data transfer phase the old instance should send the following information to the new instance:

* username in cleartext
* public key and other information about the user encrypted with the user's public key.

Once the transmission is complete, the old instance will notify the user to login to the new instance.

The data import state will hapen on the new instance. The user will login to the new instance for the purpose of making the private key available to decrypt the data received from the old instance.

Once the data has been decrypted, the new instance should iniate the handshake sequence of user authentication to verify that the public key has been transmitted successfully. The new instance will then store the information based on the user's key management type. At this point, the user should be considered authenticated and the data import phase complete. The new instance should notify the old instance to redirect queries about the user to it.

All objects created by the user should be left at the old instance where it was originally created.

#### Design

This section provides design details for implementing user authentication and authorization.

##### Credential storage

The server will store the following items:

* Username
* public key
* Other user-related information
* In the case of a hathi-managed key user, the encrypted private key. The private key should never be stored in cleartext

##### Credential validation

Credential validation will be done by verifying that a nonce encrypted with the user's public key by the Hathi Server can be echoed back by the Hathi Client re-encrypted using the user's private key.

##### Access control

Once the user has been authenticated and the Hathi client has a session token, all requests to the Hathi server should include the "session token". For every  request, the server should determine whether the request requires access control. The server must verify that all conditions are met before processing the request:

* The session token is active and associated to an active user.
* The user has the appropriate authorization to the object referenced by the UID.
* The user has the appropriate authorization to take the necessary action on the object.

If any of these conditions are not met the server will respond to indicate whether the user is not authorized or if a re-authorization is required.


Design Standards
---------------------------------------

- Microservices paradigm whenever it makes sense to modularize codebase  
  Microservice: https://en.wikipedia.org/wiki/Microservices
- Simplified html format between servers and clients in Hathi internally  
  This will simplify implementation and conserve resources, and we can bridge between
   server and ‘otherwhere’ with a bridging component
- Test Driven Development
  Consensus on using TDD paradigm
  (***TBD: As I understand TDD, this means Decide functionality, design tests for it, then implement to fulfill tests - in that order)

Data Model
---------------------------------

- Data Structure
- Representative Classes (list: names and purposes)

Functions & Processing
---------------------------------

- Data Flow and Processing
- Representative Processes (list: names, purposes, summary of functionality)

System Interfaces
---------------------------------

### Hathi Server

* POST object

* GET object

### Identity Manager

* create Identity

* delete Identity

* create Actor

* delete Actor

* grant permission to Identity for Actor

* revoke permission to Identity for Actor

### Session Manager:

* create session for Identity

* is session token valid for Operation?

### "Comms" (TBD: exposed to client):

* create Identity

* delete Identity

* create Actor

* delete Actor

* grant permission to Identity for Actor

* revoke permission to Identity for Actor

* POST object

* GET object


User Interfaces
---------------------------------

### Functionality (list of user stories)

#### Create a profile

A user should be able to create an account on a Hathi instance. The user should 
be expected to provide the following information:

* Some form of authentication credential for future use such as a username and password.
* A display name

The Hathi instance should store the salted hash of the password with the username and used to validate the user at a later time. The instance should also have a mechanism to verify the identity of the user creating the accoun. The following options are to be considered:

* Send an email with a confirmation code, to be entered by the user to the Hathi instance
* OAuth
* GPG key

Once the Hathi instance has verified the user's identity, it should provision the necessary collections as specified in [ActivityPub 5. Collections](https://www.w3.org/TR/activitypub/#collections). The user will then be able to login to the Hathi instance using the correct credentials; and the Hathi instance provide other functionality available to an existing user account.

#### Login to Hathi

A user should be able to enter the authentication credential provided when creating the account (see "Create an account" above) to login to their account on the Hathi instance. When the credential has been validated, the user is notified and the user is able to access functionality available to authenticated users.

#### Update display name

An authenticated user should be able to update the display name of their account. The user enters the new value through an interface and submits the new value. The Hathi instance should store that information and use that for cases where the user's display name is needed.

#### Update their login password

An authenticated user should be able to update their password. The user should provide their existing password and the new password. When the Hathi instance has validated that the provided existing password is correct it should replace the currently-stored salted hash with a salted hash of the new password.

At this point, the user should be expected to use the new password.

#### View an instance user's profile

Any user should be able to view another user's profile information using an ActivityPub-compliant UI.

#### Request to follow a user

An authenticated user should be able to follow another user ([ActivityPub 7.5 Follow Activity](https://www.w3.org/TR/activitypub/#follow-activity-inbox)); the other user can be on another ActivityPub instance.

If the user to follow is on the same instance, the user should be able to send the follow request while viewing the Hathi user's profile. If the user to follow is on another ActivityPub instance, the user should be able to enter the other person's ID.

#### Disposition a follow request

An authenticated user should be able to accept ([ActivityPub 7.6 Accept Activity](https://www.w3.org/TR/activitypub/#accept-activity-inbox)) or reject ([ActivityPub 7.7 Reject Activity](https://www.w3.org/TR/activitypub/#reject-activity-inbox)) another user's follow request.

#### Block a user

An authenticated user should be able to block another user either on the same instance, or another ActivityPub instance, as specified in [ActivityPub 6.9 Block Activity](https://www.w3.org/TR/activitypub/#block-activity-outbox).

If the user to block is on the same Hathi instance, the user should be able to block using the other user's ID.

#### View followers

An authenticated user should be able to view their followers list. From that list, the user should be able to navigate and get more details for anyone on that list.

#### View who they are following

An authenticated user should be able to view their following list. From that list, the user should be able to navigate and get more details for anyone on that list.

#### Create a note

An authenticated user should be able to create a note. The user should be able to decide the note's recepient as specified in [Activity Stream Section 5.1 Audience Targetting](https://www.w3.org/TR/activitystreams-vocabulary/#audienceTargeting).

If any of the receipients are in another ActivityPub instance, the Hathi instance should send the note according to [ActivityPub 7.2 Create Activity](https://www.w3.org/TR/activitypub/#create-activity-inbox).

#### Get a list of notes they wrote

An authenticated user should be able to retrieve a list of notes that they have written.

#### Edit a note

An authenticated user should be able to edit a note that they have previously created.

#### Get a list of notes they've received

An authenticated user should be able to retrieve a list of notes they have received. These notes can be sent explicitly to them; or were sent public by a user they are following.

#### View a note

A user should be able to view notes that they have been given access to by the note's creator. An authenticated user should also be able to view a note that they created on the Hathi instance.

If the note is in reply to another note, a reference to the note should be displayed.

If the note has replies, a reference to the replies should be displayed.

## Reply to a note

An authenticatee user should be able to reply to a note that they are able to view. When the Hathi instance should include the [inReplyTo property](https://www.w3.org/TR/activitystreams-vocabulary/#dfn-inreplyto) with the ID of the note being replied to as its value in the Note object.

#### Like a note

An authenticated user should be able to "like" a note. The Hathi instance should add the note's ID to the user's "liked" collection (See [ActivityPub 6.8 Like Activity](https://www.w3.org/TR/activitypub/#like-activity-outbox)).

If the note being liked is in the same instance as the user, the Hathi instance should add the user's ID to the Note's "liked" collection (See [ActivityPub 7.10 Like Activity](https://www.w3.org/TR/activitypub/#like-activity-inbox)). If the note is on another ActivityPub instance, the Hathi instance should send the Like Activity to the note creator's inbox if the "attributedTo" property is present on the note.

#### View a list of liked notes

An authenticated user should be able to view a list of notes that they have liked. From that list, the user should be able to view the ful note.

#### Record a remote user "liking" a note

When another ActivityPub instance sends an HTTP POST of a [Like Activity](https://www.w3.org/TR/activitypub/#like-activity-inbox) on an existing note ID, the Hathi instance should add the remote user's ID to the note's "likes" collection. The Hathi instance should ignore the Like Activity if the "content" property is not an ID.

#### View a count of how many times a Note has been liked

A user viewing a note should be able to see how many times a note has been liked.

#### Get a list of users who have liked a note

A user should be able to see a list of users who have liked a note. Users from both the same Hathi instance, and other ActivityPub instances, should be displayed. The user should also be able to view a user's profile from that list.

#### View a threaded note list

A user should be able to view the list of notes in a thread view.

### Text Interface

### Graphical Desktop Interface

### Web Interface

Project Evaluation
---------------------------------

- Estimation of Usability
- Adoption
