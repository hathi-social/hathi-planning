Discussion Index
==================

----------------------------------------------------------------------

Technical Platform
--------------------

[Technical Platform](./technical-platform.md)  

----------------------------------------------------------------------

Features
---------

[Users](./user-features.md)  
[Blocking](./blocking-features.md)  
[Subscriptions](./subscription-features.md)  
[Searches](./search-features.md)  
[Data handling](./datahandling-features.md)  
[Delivery scopes](./delivery-scopes.md)  
[Source differentiation](./source-differentiation.md)  
[Content filtering](./content-filtering.md)  

----------------------------------------------------------------------

Security
---------

[SSL-capability](./ssl-capability.md)  
[Multi-factor Authorization](./multifactor-authorization)  
[Identity handling](./identity-handling.md)  
[Abuse detection](abuse-detection.md)  
[Encrypted propagation layer protocol](./encrypted-propagation-layer.md)  
[Authorization](./authorization.md)  

----------------------------------------------------------------------

Interoperability
-----------------

[Interoperability](./interoperability.md)  

Notes
-----------------
[database strawman notes](./db-strawman.md)
